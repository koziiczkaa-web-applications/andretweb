import React from 'react';
import './NavBar.scss';
import {Language} from '../entities/Language';
import {SwitchButton} from './SwitchButton';
import {NavBarLogo} from './NavBarLogo';

export interface NavBarProps {
    id: string;
    language: Language[];
    onChange?: (value: boolean) => void;
}

export function NavBar(props: NavBarProps) {

    function renderItem(l: Language, index: number): JSX.Element {
        return <li key={index} className='lang-option'>
            <a className='lang-ahref' href='#'>
                <span className='lang-name'>{l.language}</span>
                <img className='lang-icon' src={l.img} alt=''/>
            </a>
        </li>;
    }

    return <header className='header' id={props.id}>
        <NavBarLogo/>
        <nav className='nav'>
            <ul className='lang-choose'>
                <SwitchButton onChange={props.onChange}/>
                {props.language.map((l: Language, index: number): JSX.Element => renderItem(l, index))}
            </ul>
        </nav>
    </header>;
}
