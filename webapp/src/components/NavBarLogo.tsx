import React from 'react';
import andret from '../../resources/images/andret.png';
import './NavBarLogo.scss';
import {Link} from 'react-router-dom';


export function NavBarLogo() {
    return <div className='logo'>
        <Link to='/'>
            <img className='andret-logo' src={andret} alt=''/>
            <p className='logo-name'>ANDRZEJ <span style={{color: '#2CBFA4'}}>"ANDRET"</span> CHMIEL <br/><span
                className='profession'>Full Stack Software Engineer</span></p>
        </Link>
    </div>;
}