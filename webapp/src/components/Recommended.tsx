import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUsers} from '@fortawesome/free-solid-svg-icons';
import './Recommended.scss';

export interface RecommendedProps {
    className?: string;
}

export function Recommended(props: React.PropsWithChildren<RecommendedProps>) {
    return <aside className={props.className}>
        <div className='recommended-title'>
            <FontAwesomeIcon icon={faUsers} size='2x' color='#2CBFA4' className='recommended-icon'/>
            <span className='recommended-text'>Polecani</span>
        </div>
        {props.children}
    </aside>;
}
