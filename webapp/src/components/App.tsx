import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import MainPage from '../pages/MainPage';
import {ProjectsPage} from '../pages/ProjectsPage';
import {NavBar} from './NavBar';
import {Footer} from './Footer';
import './App.scss';
import {projects} from '../DataProviders';
import {SingleProjectPage} from '../pages/SingleProjectPage';

export function App() {
    const [theme, setTheme] = React.useState<'dark' | 'light'>('light');

    function handleChange(value: boolean): void {
        setTheme(value ? 'dark' : 'light');
    }

    return <div className={theme + ' App'}>
        <Router>
            <main className='MainPageApp'>
                <div className='container'>
                    <NavBar
                        id='navbar'
                        onChange={handleChange}
                        language={[
                            {language: 'DE', img: '../webapp/resources/images/de-icon.png'},
                            {language: 'EN', img: '../webapp/resources/images/gb-icon.png'},
                            {language: 'PL', img: '../webapp/resources/images/pl-icon.png'}
                        ]}/>
                    <Switch>
                        <Route path='/' exact>
                            <MainPage/>
                        </Route>
                        <Route path='/projects/:id' exact>
                            <SingleProjectPage projects={projects}/>
                        </Route>
                        <Route path='/projects' exact>
                            <ProjectsPage project={projects}/>
                        </Route>
                    </Switch>
                    <Footer/>
                </div>
            </main>
        </Router>
    </div>;
}
