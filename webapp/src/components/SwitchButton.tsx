import React, {ChangeEvent} from 'react';
import './SwitchButton.scss';

export interface SwitchButtonProps {
    onChange?: (value: boolean) => void;
}

export function SwitchButton(props: SwitchButtonProps) {

    function handleChange(event: ChangeEvent<HTMLInputElement>): void {
        props.onChange?.(event.target.checked);
    }

    return <li className='li-switch'>
        <div className='switch-button'>
            <input type="checkbox" id="switch" onChange={handleChange}/>
            <label htmlFor="switch">Toggle</label>
        </div>
    </li>;
}
