import React from 'react';

import {Project} from './entities/Project';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faGitlab, faGooglePlay} from '@fortawesome/free-brands-svg-icons';
import {faFaucet} from '@fortawesome/free-solid-svg-icons';

import andret from '../resources/images/andret.png';
import alicja from '../resources/images/alicjakozik.png';
import {RecommendedItem} from './entities/RecommendedItem';


export const projects: Project[] = [
    {
        title: 'PROJECT TITLE 1',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in ligula quis arcu convallis pharetra vel nec enim. Donec euismod orci ac turpis tristique, quis mollis sem feugiat. Sed pellentesque purus id purus imperdiet cursus tincidunt a arcu.',
        img: andret,
        id: 1,
        projectReference: [
            {
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <FontAwesomeIcon icon={faGitlab} size='2x' className='icon'/>
            },
            {
                name: 'GooglePlay',
                link: 'https://google.com',
                icon: <FontAwesomeIcon icon={faGooglePlay} size='2x' className='icon'/>
            },
            {
                name: 'Spigot',
                link: 'https://spigotmc.org',
                icon: <FontAwesomeIcon icon={faFaucet} size='2x' className='icon'/>
            }
        ]
    },
    {
        title: 'PROJECT TITLE 2',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in ligula quis arcu convallis pharetra vel nec enim. Donec euismod orci ac turpis tristique, quis mollis sem feugiat. Sed pellentesque purus id purus imperdiet cursus tincidunt a arcu.',
        img: andret,
        id: 2,
        projectReference: []
    },
    {
        title: 'PROJECT TITLE 3',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in ligula quis arcu convallis pharetra vel nec enim. Donec euismod orci ac turpis tristique, quis mollis sem feugiat. Sed pellentesque purus id purus imperdiet cursus tincidunt a arcu.',
        img: andret,
        id: 3,
        projectReference: []
    },
    {
        title: 'PROJECT TITLE 4',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in ligula quis arcu convallis pharetra vel nec enim. Donec euismod orci ac turpis tristique, quis mollis sem feugiat. Sed pellentesque purus id purus imperdiet cursus tincidunt a arcu.',
        img: andret,
        id: 4,
        projectReference: []
    }
];

export const recommendedItems: RecommendedItem[] = [
    {
        img: alicja,
        name: 'Alicja Kozik',
        proffesion: 'Student & Front-End Dev',
        link: 'https://google.com'
    },
    {
        img: andret,
        name: 'Jan Kowalski',
        proffesion: 'Ćpun i frajer',
        link: 'https://google.com'
    }
];
