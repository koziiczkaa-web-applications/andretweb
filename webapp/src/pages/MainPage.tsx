import * as React from 'react';
import './MainPage.scss';
import {About} from '../components/About';
import {Main} from '../components/Main';
import {Recommended} from '../components/Recommended';
import {Quote} from '../components/Quote';
import {Contact} from '../components/Contact';
import {Projects} from '../components/Projects';

import {RecommendedComponent} from '../components/RecommendedComponent';
import {recommendedItems} from '../DataProviders';

export function MainPage() {
    return (
        <>
            <hr className='hr-classifier'/>
            <Main className='main'>
                <About className='section-about'/>
                <Projects className='projects'/>
                <Quote className='section-quote'/>
                <Contact className='section-contact'/>
            </Main>
            <Recommended className='recommended'>
                {recommendedItems.map((recommendedItem, index) => <RecommendedComponent
                    recommendedItem={recommendedItem} key={index}/>)}
            </Recommended>
            <hr className='hr-classifier'/>
        </>
    );
}

export default MainPage;
