import {ProjectReference} from './ProjectReference';

export interface Project {
    title: string;
    desc: string;
    img: string;
    id: number;
    projectReference: ProjectReference[];
}