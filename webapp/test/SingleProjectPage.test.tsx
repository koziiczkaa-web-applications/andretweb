import './__mocks__/MockReactRouterDOM'
import {shallow, ShallowWrapper} from "enzyme";
import {SingleProjectPage} from "../src/pages/SingleProjectPage";
import React, {EffectCallback} from "react";


describe('SingleProjectPage', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<SingleProjectPage projects={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('useEffect', () => {
        //given
        window.scrollTo = jest.fn();
        jest.spyOn(React, 'useEffect').mockImplementation((f: EffectCallback): void | (() => void | undefined) => f());

        //when
        const wrapper: ShallowWrapper = shallow(<SingleProjectPage projects={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>);

        //then
        expect(wrapper).toMatchSnapshot();
        expect(window.scrollTo).toHaveBeenCalledTimes(1);
        expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
    });

    it('opens image', () => {
        //given
        const wrapper: ShallowWrapper = shallow(<SingleProjectPage projects={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>);

        //when
        wrapper.find('#fscreenimg').simulate('click', {currentTarget: {src: 'andretweb'}})

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('closes image', () => {
        //given
        const wrapper: ShallowWrapper = shallow(<SingleProjectPage projects={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>);

        //when
        wrapper.find('#fscreenimg').simulate('click', {currentTarget: {src: 'andretweb'}})
        wrapper.find('#fscreenclose').simulate('click');

        //then
        expect(wrapper).toMatchSnapshot();
    });
});