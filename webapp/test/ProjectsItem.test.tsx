import {shallow, ShallowWrapper} from 'enzyme';
import {ProjectsItem} from '../src/components/ProjectsItem';
import React from 'react';

describe('ProjectsItem', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<ProjectsItem project={{
            title: 'test',
            desc: 'test123',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }}/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
