interface FactoryType {
    useParams: () => { id: string };
}

jest.mock('react-router-dom', (): FactoryType => ({
    useParams: (): { id: string } => ({
        id: '1'
    })
}));