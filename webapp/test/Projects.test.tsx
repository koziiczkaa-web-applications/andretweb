import {shallow, ShallowWrapper} from 'enzyme';
import {Projects} from '../src/components/Projects';
import React from 'react';

describe('Projects', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<Projects/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
