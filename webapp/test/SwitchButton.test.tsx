import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {SwitchButton} from '../src/components/SwitchButton';

describe('SwitchButton', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<SwitchButton/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('switches', () => {
        //given
        const handleChange: jest.Mock = jest.fn();
        const wrapper: ShallowWrapper = shallow(<SwitchButton onChange={handleChange}/>);

        //when
        wrapper.find('#switch').simulate('change', {
            target:{
                checked: true
            }
        })

        //then
        expect(wrapper).toMatchSnapshot();
        expect(handleChange).toHaveBeenCalledTimes(1);
        expect(handleChange).toHaveBeenCalledWith(true);
    });
});
