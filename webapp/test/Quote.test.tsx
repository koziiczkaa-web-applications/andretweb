import {shallow, ShallowWrapper} from 'enzyme';
import {Quote} from '../src/components/Quote';
import React from 'react';

describe('Quote', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<Quote/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
