import * as React from 'react';
import {shallow, ShallowWrapper} from 'enzyme';
import {RecommendedComponent} from '../src/components/RecommendedComponent';

describe('RecommendedComponent', () => {
    it('renders', () => {
        // when
        const wrapper: ShallowWrapper = shallow(<RecommendedComponent
            recommendedItem={{name: 'test', img: 'test', proffesion: 'test', link: 'test'}}/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });
});