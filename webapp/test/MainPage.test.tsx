import * as React from 'react';
import {shallow, ShallowWrapper} from 'enzyme';
import MainPage from '../src/pages/MainPage';

describe('MainPage', () => {
	it('renders', () => {
		//when
		const wrapper: ShallowWrapper = shallow(<MainPage/>);

		//then
		expect(wrapper).toMatchSnapshot();
	})
})
