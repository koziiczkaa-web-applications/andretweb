import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {About} from '../src/components/About';

describe('About', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<About/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
